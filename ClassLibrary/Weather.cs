﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;



namespace ClassLibrary
{
    public class Weather
    {
        public static string Temperature { private set; get; }
        public static string Wind { private set; get; }
        public static string Press { private set; get; }
        public static string ImagePath { private set; get; }

        public Weather(string temp, string wind, string press, string image)
        {
            Temperature = temp;
            Wind = wind;
            Press = press;
            ImagePath = image;
        }

        public static void SetTemp(string temp)
        {
            Temperature = temp;
        }
        public static string GetTemp()
        {
            return Temperature;
        }

        public static void SetWind(string wind)
        {
            Wind = wind;
        }
        public static string GetWind()
        {
            return Wind;
        }

        public  static void SetPress(string press)
        {
            Press = press;
        }
        public static string GetPress()
        {
            return Press;
        }

        public static void SetImagePath(string image)
        {
            ImagePath = image;
        }
        public static string GetImagePath()
        {
            return ImagePath;
        }

        public static void GetData()
        {
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            var data = client.DownloadData("https://www.gismeteo.ua/ua/weather-zhytomyr-4943/");
            var htmlCode = Encoding.UTF8.GetString(data);

        Regex regTemp = new Regex(@"<dd class='value m_temp c'>(.+)<span");
        Regex regWind = new Regex(@"<dd class='value m_wind ms' style='display:inline'>(.*)<span");
        Regex regPress = new Regex(@"<dd class='value m_press torr'>(.+)<span");
        Regex regImageUrl = new Regex("<dl class=\"cloudness\">(?:.|\\s)+url\\((.+)\\)");
        Match mTemp = regTemp.Match(htmlCode);
        Match mWind = regWind.Match(htmlCode);
        Match mPress = regPress.Match(htmlCode);
        Match mImage = regImageUrl.Match(htmlCode);

            if (mTemp.Success)  SetTemp (mTemp.Groups[1].Value);
            if (mWind.Success) SetWind( mWind.Groups[1].Value);
            if (mPress.Success) SetPress( mPress.Groups[1].Value);
            if (mImage.Success) SetImagePath ( "http:" + mImage.Groups[1].Value);
            
        }

    }
}
