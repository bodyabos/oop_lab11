﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeatherApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void labelTemperature_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonMain_Click(object sender, EventArgs e)
        {
           
            Weather.GetData();
            labelTemperature.Text = Weather.GetTemp();
            labelWind.Text = Weather.GetWind();
            labelPress.Text = Weather.GetPress();

        }
    }
}
