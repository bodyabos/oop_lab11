﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RegularCheck
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^\\+380\\d{9}|0\\d{9}$");
            Match match = reg.Match(textBox1.Text);
            if (match.Success) { label11.Text = "OK"; label11.ForeColor = Color.Green; }
            else { label11.Text = "Помилка"; label11.ForeColor = Color.Red; }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^[А-Я]{2}\\d{6}$");
            Match match = reg.Match(textBox2.Text);
            if (match.Success) { label12.Text = "OK"; label12.ForeColor = Color.Green; }
            else { label12.Text = "Помилка"; label12.ForeColor = Color.Red; }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^(1031[1-9]|103[2-9][0-9]|10[4-9][0-9]{2}|1[1-9][0-9]{3}|[2-7][0-9]{4}|8[0-8][0-9]{3}|89[0-5][0-9]{2}|896[0-3][0-9]|8964[0-5])$");
            Match match = reg.Match(textBox3.Text);
            if (match.Success) { label13.Text = "OK"; label13.ForeColor = Color.Green; }
            else { label13.Text = "Помилка"; label13.ForeColor = Color.Red; }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^[А-Я][а-я]{2,}$");
            Match match = reg.Match(textBox4.Text);
            if (match.Success) { label14.Text = "OK"; label14.ForeColor = Color.Green; }
            else { label14.Text = "Помилка"; label14.ForeColor = Color.Red; }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^([0-1]\\d|2[0-3]):[0-5]\\d$");
            Match match = reg.Match(textBox5.Text);
            if (match.Success) { label15.Text = "OK"; label15.ForeColor = Color.Green; }
            else { label15.Text = "Помилка"; label15.ForeColor = Color.Red; }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^(((\\+)|(-))(\\d+)(\\.?)(\\d+))|(((\\+)|(-)?)\\d+)$");
            Match match = reg.Match(textBox6.Text);
            if (match.Success) { label16.Text = "OK"; label16.ForeColor = Color.Green; }
            else { label16.Text = "Помилка"; label16.ForeColor = Color.Red; }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^(19[0-8][0-9]|199[0-9]|200[0-9]|201[0-7])-(0\\d|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1]\\d|2[0-3]):[0-5]\\d$");
            Match match = reg.Match(textBox7.Text);
            if (match.Success) { label17.Text = "OK"; label17.ForeColor = Color.Green; }
            else { label17.Text = "Помилка"; label17.ForeColor = Color.Red; }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^([A-Z|a-z|0-9](\\.|_){0,1})+[A-Z|a-z|0-9]\\@([A-Z|a-z|0-9])+((\\.){0,1}[A-Z|a-z|0-9]){2}\\.[a-z]{2,3}$");
            Match match = reg.Match(textBox8.Text);
            if (match.Success) { label18.Text = "OK"; label18.ForeColor = Color.Green; }
            else { label18.Text = "Помилка"; label18.ForeColor = Color.Red; }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Regex reg = new Regex("^(\\d+\\s?(грн|\\$))|(\\d+.\\d+\\s?грн)$");
            Match match = reg.Match(textBox9.Text);
            if (match.Success) { label19.Text = "OK"; label19.ForeColor = Color.Green; }
            else { label19.Text = "Помилка"; label19.ForeColor = Color.Red; }
        }
    }
}
